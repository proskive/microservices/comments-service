package de.proskive

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpDelete
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpPut
import io.ktor.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.amshove.kluent.AnyException
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should equal`
import org.amshove.kluent.`should not throw`
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.joda.time.DateTime
import java.util.*
import java.util.concurrent.TimeUnit

object ApplicationSpec : Spek({
    val apiPath = "/api/v1/test"
    val path = "http://localhost:${Config[server.port]}$apiPath/Comments"

    val engine = embeddedServer(Netty, port = Config[server.port], module = Application::test)

    val objectMapper = jacksonObjectMapper()

    // Setup Database
    Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;MODE=PostgreSQL", "org.h2.Driver", "", "")
    val flyway = Flyway()

    beforeGroup {
        flyway.setDataSource("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;MODE=PostgreSQL", "", "")
        flyway.migrate()

        engine.start(wait = false)
    }

    describe("Comment Service") {
        val notParsableJson = "This is not a parsable JSON!"

        lateinit var postResponse: Response
        var location = ""
        var created = ""

        val commentRequest = CommentRequest(
            referenceId = UUID.fromString("550e8521-e29b-51d4-a786-445655440000"),
            userId = UUID.fromString("550e8400-e29b-51d4-a786-445655440000"),
            text = "NOOOOOO?",
            tags = listOf("PM", "NOOB")
        )


        // Create a comment before each Test
        beforeEachTest {
            val (_, resp, _) = path.httpPost().setHeaders().body(objectMapper.writeValueAsString(commentRequest)).responseString()
            created = DateTime.now().toString("yyyy-MM-dd HH:mm:ss")
            location = resp.headers.getValue("Location").first()
            postResponse = resp
        }

        // Delete previously create comment after each Test
        afterEachTest {
            location.httpDelete().responseString()
        }

        describe("add comment") {
            on("empty request") {
                val (_, failResponse, _) = path.httpPost().setHeaders().responseString()

                it("should return HTTP status code 400 Bad Request") {
                    failResponse.statusCode `should be equal to` 400
                }
            }

            on("bad request") {
                val (_, failResponse, _) = path.httpPost().setHeaders().body(notParsableJson).responseString()

                it("should return HTTP status code 400 Bad Request") {
                    failResponse.statusCode `should be equal to` 400
                }
            }

            on("good request") {
                it("should return HTTP status code 201") {
                    postResponse.statusCode `should be equal to` 201
                }

                it("should return the location of the new comment") {
                    { postResponse.headers.getValue("Location").first() } `should not throw` AnyException
                }

                it("should perform get on location without exception") {
                    { location.httpGet().responseString() } `should not throw` AnyException
                }

                val (_, _, result) = location.httpGet().responseString()

                it("response json should be parsable without exception") {
                    { objectMapper.readValue<CommentResponse>(result.get()) } `should not throw` AnyException
                }

                val commentResponse = objectMapper.readValue<CommentResponse>(result.get())

                it("should create a new comment with given text") {
                    commentResponse.text `should be equal to` commentRequest.text
                }

                it("should create a new comment with given user id") {
                    commentResponse.userId.toString() `should be equal to` commentRequest.userId.toString()
                }

                it("should create a new comment with given reference id") {
                    commentResponse.referenceId.toString() `should be equal to` commentRequest.referenceId.toString()
                }

                it("should create a new comment with given tags") {
                    commentResponse.tags `should equal` commentRequest.tags
                }
            }
        }

        describe("get comment") {
            on("not existing comment") {
                val (_, failResponse, _) = "$path/999999".httpGet().setHeaders().responseString()

                it("should return HTTP status code 404 Not Found") {
                    failResponse.statusCode `should be equal to` 404
                }
            }

            on("bad request") {
                val (_, badResponse, _) = path.httpGet().setHeaders().responseString()

                it("should return HTTP status code 404 Not Found") {
                    badResponse.statusCode `should be equal to` 404
                }
            }

            on("existing comment") {
                val (_, response, result) = location.httpGet().setHeaders().responseString()

                it("should return HTTP status code 200 OK") {
                    response.statusCode `should be equal to` 200
                }

                val commentResponse = objectMapper.readValue<CommentResponse>(result.get())

                it("should return comment with given text") {
                    commentResponse.text `should be equal to` commentRequest.text
                }

                it("should return comment with given user id") {
                    commentResponse.userId.toString() `should be equal to` commentRequest.userId.toString()
                }

                it("should return comment with given reference id") {
                    commentResponse.referenceId.toString() `should be equal to` commentRequest.referenceId.toString()
                }

                it("should return comment with given tags") {
                    commentResponse.tags `should equal` commentRequest.tags
                }

                it("should return comment with right creation time") {
                    commentResponse.createdAt `should be equal to` created
                }

                it("should return comment with no update time") {
                    commentResponse.updatedAt ?: "" `should be equal to` ""
                }
            }
        }

        describe("update comment") {
            on("not existing comment") {
                val (_, failResponse, _) = "$path/999999".httpPut().setHeaders().body(
                    objectMapper.writeValueAsString(
                        commentRequest.copy(
                            text = "This is a updated comment"
                        )
                    )
                ).responseString()

                it("should return HTTP status code 404 Not Found") {
                    failResponse.statusCode `should be equal to` 404
                }
            }

            on("bad request") {
                val (_, badResponse, _) = location.httpPut().setHeaders().responseString()

                it("should return HTTP status code 400 Bad Request") {
                    badResponse.statusCode `should be equal to` 400
                }
            }

            on("existing comment") {
                val commentUpdate = CommentRequest(
                    referenceId = UUID.fromString("550c8551-e26b-51d7-a788-445655440120"),
                    userId = UUID.fromString("550e8400-e29b-51d4-a786-445655441234"),
                    text = "This is a updated comment",
                    tags = listOf("PRO")
                )

                val (_, response, _) = location.httpPut().setHeaders().body(
                    objectMapper.writeValueAsString(
                        commentUpdate
                    )
                ).responseString()
                val update = DateTime.now().toString()

                it("should return HTTP status code 201") {
                    response.statusCode `should be equal to` 201
                }

                it("get on location should perform without exception") {
                    { location.httpGet().responseString() } `should not throw` AnyException
                }

                val (_, _, updateResult) = location.httpGet().setHeaders().responseString()

                it("response json should be parsable without exception") {
                    { objectMapper.readValue<CommentResponse>(updateResult.get()) } `should not throw` AnyException
                }

                val commentResponse = objectMapper.readValue<CommentResponse>(updateResult.get())

                it("should return updated comment with given text") {
                    commentResponse.text `should be equal to` commentUpdate.text
                }

                it("should return comment with given user id") {
                    commentResponse.userId.toString() `should be equal to` commentUpdate.userId.toString()
                }

                it("should return comment with given reference id") {
                    commentResponse.referenceId.toString() `should be equal to` commentUpdate.referenceId.toString()
                }

                it("should return comment with given tags") {
                    commentResponse.tags `should equal` commentUpdate.tags
                }

                it("should return comment with right creation time") {
                    commentResponse.createdAt `should be equal to` created
                }

                it("should return comment with right updated time") {
                    commentResponse.updatedAt ?: "" `should be equal to` update
                }
            }
        }

        describe("delete comment") {
            on("not existing comment") {
                val (_, failResponse, _) = "$path/999999".httpDelete().setHeaders().responseString()

                it("should return HTTP status code 404 Not Found") {
                    failResponse.statusCode `should be equal to` 404
                }
            }

            on("bad request") {
                val (_, badResponse, _) = path.httpDelete().setHeaders().responseString()

                it("should return HTTP status code 404 Not Found") {
                    badResponse.statusCode `should be equal to` 404
                }
            }

            on("existing comment") {
                val comments = HashMap<Int, String>()

                it("should return HTTP status code 204") {
                    val (_, response, _) = location.httpDelete().setHeaders().responseString()
                    response.statusCode `should be equal to` 204
                }

                it("should not find deleted comment") {
                    val (_, response, _) = location.httpGet().setHeaders().responseString()
                    response.statusCode `should be equal to` 404
                }

                for (i in 1..3) {
                    val comment = commentRequest.copy(text = i.toString())
                    val (_, response, _) = path.httpPost().setHeaders().body(objectMapper.writeValueAsString(comment)).responseString()
                    comments[i] = response.headers.getValue("Location").first()
                }

                comments[2]?.httpDelete()?.responseString()

                it("should return not deleted 1. comment") {
                    val (_, response, _) = (comments[1] ?: "").httpGet().setHeaders().responseString()
                    response.statusCode `should be equal to` 200
                }

                it("should not return deleted 2. comment") {
                    val (_, response, _) = (comments[2] ?: "").httpGet().setHeaders().responseString()
                    response.statusCode `should be equal to` 404
                }

                it("should return not deleted 3. comment") {
                    val (_, response, _) = (comments[3] ?: "").httpGet().setHeaders().responseString()
                    response.statusCode `should be equal to` 200
                }
            }
        }
    }

    afterGroup {
        engine.stop(1, 1, TimeUnit.SECONDS)
    }
})

fun Request.setHeaders(): Request {
    this.headers.clear()
    this.header("Accept" to "application/json")
    this.header("Content-Type" to "application/json")
    return this
}
