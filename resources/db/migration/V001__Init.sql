-- noinspection SqlNoDataSourceInspectionForFile

CREATE TABLE Comments (
  id           BIGSERIAL PRIMARY KEY,
  reference_id uuid         NOT NULL,
  group_id     VARCHAR(256) NOT NULL,
  user_id      uuid         NOT NULL,
  text         TEXT         NOT NULL,
  created      TIMESTAMP    NOT NULL,
  updated      TIMESTAMP    NULL
);

CREATE TABLE Tags (
  id      BIGSERIAL PRIMARY KEY,
  comment BIGINT       NOT NULL,
  tag     VARCHAR(256) NOT NULL,
  FOREIGN KEY (comment) REFERENCES Comments (id) ON DELETE RESTRICT ON UPDATE RESTRICT
);
