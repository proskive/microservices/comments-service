# ProSkive.Services.Comments

[![Docker Stars](https://img.shields.io/docker/stars/proskive/comments-service.svg)](https://hub.docker.com/r/proskive/comments-service) [![Docker Pulls](https://img.shields.io/docker/pulls/proskive/comments-service.svg)](https://hub.docker.com/r/proskive/comments-service) [![Docker Automated build](https://img.shields.io/docker/automated/proskive/comments-service.svg)](https://hub.docker.com/r/proskive/comments-service) [![Image](https://images.microbadger.com/badges/image/proskive/comments-service.svg)](https://microbadger.com/images/proskive/comments-service) [![Version](https://images.microbadger.com/badges/version/proskive/comments-service.svg)](https://microbadger.com/images/proskive/comments-service)

### Prerequisites

- Postgres
- JWK Provider (OpenID Connect + JWT)

### Configuration

The Configuration is implemented with [konfig](https://github.com/npryce/konfig). To configure this service you can 
either specify environment variables, an properties file or a combination of both. The Hierarchy is like following (-> = overriding):

System Properties -> Environment Variables -> Properties File -> Default Properties File

The properties files can be placed in `/etc/` or in the `working directory` and must be named `comment-service.properties`. Following you will see all properties and defaults:

```properties
server.port = 4567

postgres.host = // Postgres database host
postgres.port = 5432
postgres.user = proskive
postgres.password = proskive
postgres.database = comments

eureka.enabled = true
eureka.host.scheme = http
eureka.host.domain = // Eureka Domain e.g. localhost
eureka.host.port = // Eureka Port
eureka.instance.id = // Service ID
eureka.instance.name = COMMENT-SERVICE
eureka.instance.hostname = localhost
eureka.instance.ip = // By default IP of service
eureka.instance.vip = comment-service
eureka.instance.port = 4567
eureka.instance.data-center-name = MyOwn

authentication.enabled = true
authentication.realm = Comments
authentication.jwk.scheme = https
authentication.jwk.domain = // JWK Domain e.g. localhost
authentication.jwk.port = // JWK Port e.g. 8080
authentication.jwk.path = // Path to jwk e.g. in local Keycloak /auth/realms/Demo/protocol/openid-connect/certs
authentication.required-roles = // Currently only Keycloak roles e.g. ROLE_PROSKIVE_ADMIN, ROLE_USER

metrics.enabled = false // Currently not tested
```

All properties can be passed as environment variables. For that you need to use upper-case-and-underscores convention. For example ```POSTGRES_HOST``` or ```AUTHENTICATION_REQUIRED_ROLES```. 

### Database

An example for an compatible docker container:

```docker
docker run --name comments-db -e POSTGRES_USER=proskive -e POSTGRES_PASSWORD=proskive -e POSTGRES_DB=comments -p 5432:5432 -d postgres:10.5-alpine
```

### Service Discovery
[Spring Initializer](https://start.spring.io) with dependency `Eureka Server` and `Eureka Discovery`

## Getting Started

### Authentication

To make request you need to get an access token from your JWK Provider. In this example with keycloak:

- POST http://{KEYCLOAK_HOST_PORT}/auth/realms/{REALM}/protocol/openid-connect/token
- Content-Type: application/x-www-form-urlencoded
- Accept: application/json

client_id=token&grant_type=password&username={USERNAME}&password={PASSWORD}

### API

You can find the [Open API](https://swagger.io/resources/open-api/) of this service in the `api.yaml` file in the root directory of this project. To view this file you can use [Swagger Editor](https://github.com/swagger-api/swagger-editor).
```
docker run -d -p 80:8080 swaggerapi/swagger-editor
```
This will run Swagger Editor (in detached mode) on port 80 on your machine, so you can open it by navigating to `http://localhost` in your browser.