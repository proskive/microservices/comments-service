package de.proskive

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpPut
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import mu.KotlinLogging
import java.net.ConnectException
import java.net.InetAddress

class EurekaClientException(message: String, exception: Exception) : Exception(message, exception)

data class EurekaResource(val instance: EurekaInstanceResource)
data class EurekaInstanceResource(val hostName: String,
                                  val app: String,
                                  @JsonProperty("ipAddr")
                                  val ipAddress: String,
                                  val vipAddress: String,
                                  val secureVipAddress: String,
                                  val status: EurekaClientStatus = EurekaClientStatus.UP,
                                  val port: EurekaPort,
                                  val securePort: EurekaPort,
                                  val homePageUrl: String,
                                  val statusPageUrl: String,
                                  val healthCheckUrl: String,
                                  val dataCenterInfo: EurekaDataCenterInfoResource,
                                  val metadata: EurekaMetadataResource)

data class EurekaPort(@JsonProperty("$") val number: Int, @JsonProperty("@enabled") val enabled: String)
data class EurekaDataCenterInfoResource(@JsonProperty("@class") val classPath: String, val name: String)
data class EurekaMetadataResource(val instanceId: String)

enum class EurekaClientStatus {
    UP, DOWN, STARTING, OUT_OF_SERVICE, UNKNOWN;
}

object Time {
    const val SECONDS: Int = 1000
}

private val logger = KotlinLogging.logger { }

class EurekaClient(private val apiPath: String) {
    private val eurekaHostUrl = "${Config[eureka.host.scheme].removeSuffix("://")}://${Config[eureka.host.domain]}:${Config[eureka.host.port]}/eureka/apps/${Config[eureka.instance.name]}"
    private val registerDelay = 60
    private val heartbeatDelay = 300

    fun register() = launch {

        val ip = if (Config[eureka.instance.ip].isNotBlank()) Config[eureka.instance.ip] else InetAddress.getLocalHost().hostAddress

        val eurekaResource = EurekaResource(
            EurekaInstanceResource(
                hostName = Config[eureka.instance.hostname],
                app = Config[eureka.instance.name].toUpperCase(),
                ipAddress = ip,
                vipAddress = Config[eureka.instance.vip],
                secureVipAddress = ip,
                status = EurekaClientStatus.UP,
                port = EurekaPort(
                    number = Config[server.port],
                    enabled = "true"
                ),
                securePort = EurekaPort(
                    number = Config[server.port],
                    enabled = "false"
                ),
                homePageUrl = "http://${Config[eureka.instance.hostname]}:${Config[server.port]}/",
                statusPageUrl = "http://${Config[eureka.instance.hostname]}:${Config[server.port]}$apiPath/info",
                healthCheckUrl = "http://${Config[eureka.instance.hostname]}:${Config[server.port]}$apiPath/health",
                dataCenterInfo = EurekaDataCenterInfoResource(
                    classPath = "com.netflix.appinfo.InstanceInfo\$DefaultDataCenterInfo",
                    name = Config[eureka.instance.data_center_name]
                ),
                metadata = EurekaMetadataResource(
                    instanceId = Config[eureka.instance.id]
                )
            )
        )

        val eurekaResourceJson = jacksonObjectMapper().writeValueAsString(eurekaResource)

        logger.info { "Register on Eureka with: $eurekaHostUrl" }

        var isRegistered = false

        while (!isRegistered) {

            val (_, response, result) = eurekaHostUrl.httpPost()
                .header(mapOf("Content-Type" to "application/json"))
                .body(eurekaResourceJson)
                .response()

            result.fold({
                logger.info { "Successfully registered with Eureka (${response.url})! Starting heartbeat in $heartbeatDelay sec. ..." }
                sendHeartbeatToEureka()
                isRegistered = true
            }, { error ->
                logger.warn { "Could not register with Eureka: ${error.message}" }
                logger.warn { "Retry register with Eureka in $registerDelay seconds." }
                delay(registerDelay * Time.SECONDS)
            })
        }
    }

    private suspend fun sendHeartbeatToEureka() {
        var isRegistered = true

        while (isRegistered) {
            delay(heartbeatDelay * Time.SECONDS)

            logger.info { "Send heartbeat ..." }

            try {
                val (_, _, result) = "$eurekaHostUrl/${Config[eureka.instance.hostname]}:${Config[server.port]}".httpPut().response()

                result.fold({}, { error ->
                    logger.warn { "Could not send heartbeat: ${error.message}" }
                    isRegistered = false
                })
            } catch (ex: ConnectException) {
                logger.warn { "Could not send heartbeat: ${ex.message}" }
                isRegistered = false
            }
        }

        register()
    }
}