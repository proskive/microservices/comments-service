package de.proskive

import java.util.*

fun String.toUUID(): UUID {
    return UUID.fromString(this)
}