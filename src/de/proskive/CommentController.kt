package de.proskive

import org.jetbrains.exposed.sql.SizedIterable
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.util.*

object CommentController {
    fun group(group: String) = CommentGroupController(group)

    class CommentGroupController(private val group: String) {

        fun create(commentJson: CommentRequest): Long {
            return transaction {
                val comment = Comment.new {
                    referenceId = commentJson.referenceId
                    groupId = group
                    userId = commentJson.userId
                    text = commentJson.text
                }

                for (tag in commentJson.tags) {
                    Tag.new {
                        this.comment = comment
                        this.tag = tag
                    }
                }

                return@transaction comment.id.value
            }
        }

        private fun read(id: Long): Comment? {
            return transaction {
                Comment.find { (Comments.id eq id) and (Comments.groupId eq group) }.firstOrNull()
            }
        }

        fun readToJson(id: Long): CommentResponse? {
            return transaction {
                read(id)?.toJson()
            }
        }

        private fun readByReferenceId(referenceId: UUID): SizedIterable<Comment> {
            return transaction {
                Comment.find { (Comments.referenceId eq referenceId) and (Comments.groupId eq group) }
            }
        }

        fun readByReferenceIdToJson(referenceId: UUID): List<CommentResponse> {
            return transaction {
                readByReferenceId(referenceId).map { it.toJson() }
            }
        }

        fun update(id: Long, commentJson: CommentRequest): Long? {
            return transaction {
                val comment = read(id)

                if (comment != null) {
                    comment.tags.forEach { tag -> tag.delete() }

                    for (tag in commentJson.tags) {
                        Tag.new {
                            this.comment = comment
                            this.tag = tag
                        }
                    }

                    comment.apply {
                        referenceId = commentJson.referenceId
                        userId = commentJson.userId
                        text = commentJson.text
                        updatedAt = DateTime.now()
                    }.id.value
                } else {
                    null
                }
            }
        }

        fun delete(id: Long): Boolean {
            return transaction {
                val comment = read(id)
                comment?.tags?.forEach { tag -> tag.delete() }
                comment?.delete()
                return@transaction comment != null
            }
        }

        fun deleteByReferenceId(referenceId: UUID): Boolean {
            return transaction {
                val comments = readByReferenceId(referenceId)
                val notFound = comments.empty()

                comments.forEach { comment ->
                    delete(comment.id.value)
                }

                return@transaction !notFound
            }
        }
    }
}