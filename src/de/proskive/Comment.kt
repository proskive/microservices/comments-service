package de.proskive

import com.fasterxml.jackson.annotation.JsonProperty
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.LongIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.sql.Connection
import java.util.*

object Comments : LongIdTable() {
    val referenceId = uuid("reference_id")
    val groupId = varchar("group_id", 256)
    val userId = uuid("user_id")
    val text = text("text")
    val createdAt = datetime("created").clientDefault { DateTime.now() }
    val updatedAt = datetime("updated").nullable()
}

class Comment(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Comment>(Comments)

    var referenceId by Comments.referenceId
    var groupId by Comments.groupId
    var userId by Comments.userId
    var text by Comments.text
    var createdAt by Comments.createdAt
    var updatedAt by Comments.updatedAt

    val tags by Tag referrersOn Tags.comment

    fun toJson(): CommentResponse {
        val comment = this
        return transaction {
            CommentResponse(
                id = comment.id.value,
                groupId = comment.groupId,
                referenceId = comment.referenceId,
                userId = comment.userId,
                tags = comment.tags.map { it.tag },
                text = comment.text,
                createdAt = comment.createdAt.toString("yyyy-MM-dd HH:mm:ss"),
                updatedAt = comment.updatedAt?.toString("yyyy-MM-dd HH:mm:ss")
            )
        }
    }
}

object Tags : LongIdTable() {
    val comment = reference("comment", Comments)
    val tag = varchar("tag", 256)
}


class Tag(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Tag>(Tags)

    var comment by Comment referencedOn Tags.comment
    var tag by Tags.tag
}

data class CommentRequest(
    @JsonProperty("reference_id")
    val referenceId: UUID,
    @JsonProperty("user_id")
    val userId: UUID,
    val tags: List<String> = emptyList(),
    val text: String = ""
)

data class CommentResponse(
    val id: Long,
    @JsonProperty("group_id")
    val groupId: String,
    @JsonProperty("reference_id")
    val referenceId: UUID,
    @JsonProperty("user_id")
    val userId: UUID,
    val tags: List<String>,
    val text: String,
    @JsonProperty("created_at")
    val createdAt: String,
    @JsonProperty("updated_at")
    val updatedAt: String? = null
)
