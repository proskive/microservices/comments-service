package de.proskive

import com.auth0.jwk.UrlJwkProvider
import com.codahale.metrics.Slf4jReporter
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.application.log
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.JWTPrincipal
import io.ktor.auth.jwt.jwt
import io.ktor.features.AutoHeadResponse
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.jackson.jackson
import io.ktor.metrics.Metrics
import io.ktor.request.path
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.experimental.timeunit.TimeUnit
import org.slf4j.event.Level
import java.net.URL
import java.time.Duration

fun main(args: Array<String>) {
    Database.init()

    embeddedServer(Netty, port = Config[server.port], module = Application::main).start(wait = false)

    if (Config[eureka.enabled]) {
        EurekaClient("/api/v1").register()
    }
}

fun Application.main() {
    this.module()
}

fun Application.test() {
    this.module(true)
}

fun Application.module(test: Boolean = false) {
    install(AutoHeadResponse)

    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)

        header(HttpHeaders.Authorization)
        header(HttpHeaders.AccessControlAllowOrigin)

        header(HttpHeaders.XForwardedProto)
        host("*")

        allowSameOrigin = true
        allowCredentials = true
        maxAge = Duration.ofDays(1)
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    if (Config[metrics.enabled]) {
        install(Metrics) {
            Slf4jReporter.forRegistry(registry)
                .outputTo(log)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build()
                .start(10, TimeUnit.SECONDS)
        }
    }

    if (Config[authentication.enabled] && !test) {
        install(Authentication) {
            jwt {
                realm = Config[authentication.realm]
                verifier(
                    UrlJwkProvider(
                        URL(
                            "${Config[authentication.jwk.scheme].removeSuffix("://")}://${Config[authentication.jwk.domain]}:${Config[authentication.jwk.port]}/${Config[authentication.jwk.path].removePrefix(
                                "/"
                            )}"
                        )
                    )
                )
                validate { credential ->
                    if (credential.payload.getClaim("realm_access").asMap()["roles"].let {
                            it is List<*> && it.any { role ->
                                Config[authentication.required_roles].contains(role)
                            }
                        }) JWTPrincipal(credential.payload) else null
                }
            }
        }
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT) // Pretty Prints the JSON
        }
    }

    routing {
        if (Config[authentication.enabled] && !test) {
            main()
        } else {
            test()
        }
    }
}