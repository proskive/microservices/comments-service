package de.proskive

import com.natpryce.konfig.*
import java.io.File

object server : PropertyGroup() {
    val port by intType
}

object postgres : PropertyGroup() {
    val host by stringType
    val port by intType
    val user by stringType
    val password by stringType
    val database by stringType
}

object eureka : PropertyGroup() {
    val enabled by booleanType

    object host : PropertyGroup() {
        val scheme by stringType
        val domain by stringType
        val port by stringType
    }

    object instance : PropertyGroup() {
        val id by stringType
        val name by stringType
        val hostname by stringType
        val ip by stringType
        val vip by stringType
        val data_center_name by stringType
    }
}

object authentication : PropertyGroup() {
    val enabled by booleanType
    val realm by stringType
    object jwk : PropertyGroup() {
        val scheme by stringType
        val domain by stringType
        val port by intType
        val path by stringType
    }
    val required_roles by listType(stringType)
}

object metrics: PropertyGroup() {
    val enabled by booleanType
}

object Config: Configuration {
    private val instance by lazy { loadConfig() }

    override fun <T> getOrNull(key: Key<T>): T? {
        return instance.getOrNull(key)
    }

    override fun list(): List<Pair<Location, Map<String, String>>> {
        return instance.list()
    }

    override fun locationOf(key: Key<*>): PropertyLocation? {
        return instance.locationOf(key)
    }

    override fun searchPath(key: Key<*>): List<PropertyLocation> {
        return instance.searchPath(key)
    }

    private fun loadConfig(): Configuration {
        return ConfigurationProperties.systemProperties() overriding
                EnvironmentVariables() overriding
                (when {
                    File("/etc/comment-service.properties").exists() -> ConfigurationProperties.fromFile(File("/etc/comment-service.properties"))
                    File("comment-service.properties").exists() -> ConfigurationProperties.fromFile(File("comment-service.properties"))
                    else -> EmptyConfiguration
                }) overriding
                ConfigurationProperties.fromResource("config/defaults.properties")
    }
}