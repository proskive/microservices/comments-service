package de.proskive

import mu.KotlinLogging
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.postgresql.util.PSQLException
import java.net.ConnectException
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

private val logger = KotlinLogging.logger { }

object Database {
    private val postgresUrl = "jdbc:postgresql://${Config[postgres.host]}:${Config[postgres.port]}"
    private val databaseUrl = "$postgresUrl/${Config[postgres.database]}"
    private const val databaseDriver = "org.postgresql.Driver"

    val connection by lazy {
        initDatabase()
    }

    fun init() {
        connection
    }

    private fun initDatabase(): Database {
        // Create Database if not exist
        Class.forName(databaseDriver)

        var connectionSuccessful = false
        lateinit var connection: Connection

        while (!connectionSuccessful) {
            try {
                connection = DriverManager.getConnection(
                    "$postgresUrl/postgres",
                    Config[postgres.user],
                    Config[postgres.password]
                )
                connectionSuccessful = true
            } catch (ex: Exception) {
                logger.info { "Could not connect to Postgres retry in 5 seconds" }
                Thread.sleep(5_000)
            }
        }

        try {
            connection.createStatement().executeUpdate("CREATE DATABASE ${Config[postgres.database]}")
        } catch (ex: SQLException) {
            if (ex.errorCode == 1007) {
                logger.info { "Database ${Config[postgres.database]} already exist" }
            } else {
                logger.error(ex) {  }
            }
        }

        // Connect to Database
        val database = Database.connect(
            url = databaseUrl,
            driver = databaseDriver,
            user = Config[postgres.user],
            password = Config[postgres.password]
        )

        // Migrate Database
        val flyway = Flyway()
        flyway.setDataSource(databaseUrl, Config[postgres.user], Config[postgres.password])
        flyway.migrate()

        return database
    }
}