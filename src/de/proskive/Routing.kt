package de.proskive

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.authenticate
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.pipeline.PipelineContext
import io.ktor.request.receive
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.routing.*
import mu.KotlinLogging
import org.jetbrains.exposed.sql.transactions.transaction

data class StatusResponse(
    val status: HttpStatusCode,
    val message: String? = null
)

private val logger = KotlinLogging.logger { }

private const val API_PATH = "/api/v1"
private const val COMMENTS_PATH = "/{group?}/Comments"

fun Routing.main() {
    authenticate {
        root()
    }

}

fun Routing.test() {
    root()
}

private fun Route.root() {
    // TODO: Make group optional
    route(API_PATH) {
        get("info") {
            call.respond(HttpStatusCode.OK, mapOf("name" to "comment-service"))
        }

        get("health") {
            call.respond(HttpStatusCode.OK, mapOf("status" to "UP"))
        }

        route(COMMENTS_PATH) {

            // TODO: Delete after https://github.com/ktorio/ktor/issues/89 is implemented
            options {
                call.respond(HttpStatusCode.OK)
            }

            options("{id}") {
                call.respond(HttpStatusCode.OK)
            }

            options("byReferenceId/{id}") {
                call.respond(HttpStatusCode.OK)
            }

            post {
                val group = call.parameters["group"] ?: ""
                val commentRequest = call.receive<CommentRequest>()

                val commentId = CommentController.group(group).create(commentRequest)

                call.response.header("Location", location(commentId))
                call.respondWithStatusMessage(HttpStatusCode.Created, location(commentId))
            }

            get("{id}") {
                val group = call.parameters["group"] ?: ""
                val requestId = call.parameters["id"]?.toLong() ?: throw UrlParseException()

                val result = CommentController.group(group).readToJson(requestId)

                if (result != null) {
                    call.respond(HttpStatusCode.OK, result)
                } else {
                    call.respondWithStatusMessage(
                        HttpStatusCode.NotFound,
                        "Could not find Comment with id '$requestId' and group_id '$group'"
                    )
                }
            }

            get("byReferenceId/{id}") {
                val group = call.parameters["group"] ?: ""
                val requestId = call.parameters["id"]?.toUUID() ?: throw UrlParseException()

                val result = CommentController.group(group).readByReferenceIdToJson(requestId)

                if (transaction { !result.isEmpty() }) {
                    call.respond(HttpStatusCode.OK, result)
                } else {
                    call.respondWithStatusMessage(
                        HttpStatusCode.NotFound,
                        "Could not find Comments with reference_id '$requestId' and group_id '$group'"
                    )
                }
            }

            put("{id}") {
                val group = call.parameters["group"] ?: ""
                val requestId = call.parameters["id"]?.toLong() ?: throw UrlParseException()
                val commentRequest = call.receive<CommentRequest>()

                val result = CommentController.group(group).update(requestId, commentRequest)

                if (result != null) {
                    call.response.header("Location", location(requestId))
                    call.respondWithStatusMessage(HttpStatusCode.Created, location(result))
                } else {
                    call.respondWithStatusMessage(
                        HttpStatusCode.NotFound,
                        "Could not find Comment with id '$requestId' and group_id '$group'"
                    )
                }
            }

            delete("{id}") {
                val group = call.parameters["group"] ?: ""
                val requestId = call.parameters["id"]?.toLong() ?: throw UrlParseException()

                val successful = CommentController.group(group).delete(requestId)

                if (successful) {
                    call.respond(HttpStatusCode.NoContent)
                } else {
                    call.respondWithStatusMessage(
                        HttpStatusCode.NotFound, "Could not find Comment with id '$requestId' and group_id '$group'"
                    )
                }
            }

            delete("byReferenceId/{id}") {
                val group = call.parameters["group"] ?: ""
                val requestId = call.parameters["id"]?.toUUID() ?: throw UrlParseException()

                val successful = CommentController.group(group).deleteByReferenceId(requestId)

                if (successful) {
                    call.respond(HttpStatusCode.NoContent)
                } else {
                    call.respondWithStatusMessage(
                        HttpStatusCode.NotFound, "Could not find Comments with id '$requestId' and group_id '$group'"
                    )
                }
            }
        }
    }


    install(StatusPages) {
        exception<UrlParseException> { cause ->
            call.respondWithStatusMessage(HttpStatusCode.BadRequest, "Please check the URL")
            logger.debug(cause) {}
        }
        exception<JsonParseException> { cause ->
            call.respond(HttpStatusCode.BadRequest, "Content does not conform to JSON syntax as per specification")
            logger.debug(cause) {}
        }
        exception<JsonMappingException> { cause ->
            call.respond(HttpStatusCode.BadRequest, "Please check your JSON")
            logger.debug(cause) {}
        }
        exception<AuthenticationException> { cause ->
            call.respond(HttpStatusCode.Unauthorized)
            logger.debug(cause) {}
        }
        exception<AuthorizationException> { cause ->
            call.respond(HttpStatusCode.Forbidden)
            logger.debug(cause) {}
        }
        exception<IllegalStateException> { cause ->
            call.respond(HttpStatusCode.InternalServerError)
            logger.error(cause) {}
        }
    }
}

fun PipelineContext<Unit, ApplicationCall>.location(id: Long): String {
    val local = this.call.request.local
    val group = this.call.parameters["group"]?.plus("/") ?: ""
    return "${local.scheme}://${local.host}:${local.port}$API_PATH${COMMENTS_PATH.replace("{group?}/", group)}/$id"
}

suspend fun ApplicationCall.respondWithStatusMessage(status: HttpStatusCode, message: String) {
    this.respond(status, StatusResponse(status, message))
}

class UrlParseException : RuntimeException()
class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()